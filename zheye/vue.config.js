module.exports = {
    runtimeCompiler: true,
    productionSourceMap: false,
    css: {
        sourceMap: false,
        loaderOptions: {
            less: {javascriptEnabled: true}
        }
    },
    publicPath:'/zheye',
    devServer: {
        port: 5001,
        proxy: {
            '/api': {
                target: 'http://175.24.124.48:3300/', // 本地
                ws: true,
                changeOrigin: true
            }
        }
    }
};
