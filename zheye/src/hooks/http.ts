import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import store from '../store'
import createMessage from '@/components/createMessage'

let config = {
    // baseURL: process.env.baseURL || process.env.apiUrl || ""
    headers: {
        get: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        post: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    },
    // 是否跨站点访问控制请求
    withCredentials: true,
    timeout: 30000,
};
const service = axios.create(config);

// 请求拦截器
service.interceptors.request.use((config: AxiosRequestConfig) => {
    // 获取token，并将其添加至请求头中
    let token = localStorage.getItem("token")
    if(token){
        config.headers.Authorization = `Bearer ${token}`;
    }
    store.commit('setLoading', true)
    store.commit('setError', { status: false, message: '' })
    return config
}, (error: any) => {
    // 错误抛到业务代码
    error.data = {}
    error.data.msg = '服务器异常，请联系管理员！'
    return Promise.resolve(error)
})

// 响应拦截器
service.interceptors.response.use((response: AxiosResponse) => {
    if (response.data && response.data.code === 200) {
        response.data.code = 0
    } else  {
        response.data.code = 1
    }
    store.commit('setLoading', false)
    return response.data;
}, (error:any) => {
    // console.log('error--->',error.response)
    if (axios.isCancel(error)) {
        console.log('repeated request: ' + error.message)
    } else {
        store.commit('setError', { status: true, message: '请求超时或服务器异常，请检查网络或联系管理员！' })
        store.commit('setLoading', false)
    }

    return Promise.reject(error)
})

export default service
