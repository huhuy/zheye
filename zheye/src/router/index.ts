import {createRouter, createWebHistory,createWebHashHistory , RouteRecordRaw} from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'
import {UserService} from "@/api";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login.vue'),
        meta: { redirectAlreadyLogin: true }
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('@/views/Register.vue')
    },
    {
        path: '/create',
        name: 'Create',
        component: () => import('@/views/CreateArticle.vue'),
        meta: { requiredLogin: true }
    },
    {
        path: '/column/:id',
        name: 'Column',
        component: () => import('@/views/ColumnList.vue')
    },
    {
        path: '/details/:id',
        name: 'Details',
        component: () => import('@/views/ColumnDetails.vue')
    },
    {
        path: '/edit',
        name: 'Edit',
        component: () => import('@/views/UserEdit.vue')
    }
]
console.log('---->',process.env.BASE_URL)
const router = createRouter({
    // history: createWebHistory(process.env.BASE_URL),
    history: createWebHistory('/zheye'),
    routes
})

router.beforeEach((to, from, next) => {
    const { user, token } = store.state
    const { requiredLogin, redirectAlreadyLogin } = to.meta
    if (!user.isLogin) {
        if (token) {
            UserService.currentUser().then(res => {
                store.commit('login', res.data)
                if (redirectAlreadyLogin) {
                    next('/')
                } else {
                    next()
                }
            }).catch(e => {
                console.error(e)
                store.commit('logout')
                next('login')
            })
        } else {
            if (requiredLogin) {
                next('login')
            } else {
                next()
            }
        }
    } else {
        if (redirectAlreadyLogin) {
            next('/')
        } else {
            next()
        }
    }
})

export default router
