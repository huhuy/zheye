import axios from "@/hooks/http";
import store from "@/store";

export interface ResponseType<P = {}> {
    code: number;
    msg: string;
    data: P;
}

/***
 * @interface LoginParams - 注册参数
 * @email {string} email - 邮箱
 * @property {string} username - 用户名
 * @property {string} password - 用户密码
 */
export interface LoginParams {
    email: string
    username?: string
    password: string
}

export interface UserParams {
    nickName?: string;
    avatar?: string;
    description?: string;
}

//封装User类型的接口方法
export class UserService {
    /**
     * 登录
     */
    static async login(params: LoginParams): Promise<any> {
        return axios.post('/api/user/login', params)
    }

    /**
     * 注册用户
     */
    static async register(params: LoginParams): Promise<any> {
        return axios.post('/api/user/register', params)
    }

    /**
     * 获取当前用户信息
     */
    static async currentUser(): Promise<any> {
        return axios.get(`/api/user/current`)
    }

    /**
     * 更新用户信息
     * @param params
     * @param id
     */
    static async updateUser(params: UserParams, id: string): Promise<ResponseType> {
        return axios.patch(`/api/user/update/${id}`, params)
    }
}

export interface ColumnProps {
    id: string;
    title: string;
    avatar?: string;
    description: string;
}

export interface LoadParams {
    page: number;
    size: number;
}

export class ColumnServer {
    /**
     * 专栏列表
     * @param query
     */
    static async columnList(query: LoadParams): Promise<any> {
        return axios.get(`/api/category/list`, {params: query})
    }

    /**
     * 专栏详情
     */
    static async fetchColumn(id: string): Promise<any> {
        return axios.get(`/api/category/detail/${id}`)
    }

    /**
     * 更新专栏信息
     * @param params
     * @param id
     */
    static async updateColumn(params: UserParams, id: string): Promise<ResponseType> {
        return axios.patch(`/api/category/update/${id}`, params)
    }
}


export interface PostProps {
    id?: string;
    title: string;
    excerpt?: string;
    content?: string;
    image?: string;
    columnId: string;
    author?: string;
    isHTML?: boolean;
}

export class ArticleServer {
    /**
     * 专题下文章列表
     * @param query
     * @param id
     */
    static async articleList(query: LoadParams, id: string): Promise<any> {
        return axios.get(`/api/article/${id}/list`, {params: query})
    }

    /**
     * 添加文章
     * @param params
     */
    static async articleAdd(params: PostProps): Promise<any> {
        return axios.post('/api/article/add', params)
    }

    /**
     * 更新文章
     * @param params
     */
    static async articleUpdate(params: PostProps, id: string): Promise<ResponseType> {
        return axios.put(`/api/article/update/${id}`, params)
    }

    /**
     * 获取文章详情
     * @param id
     */
    static async articleDetail(id: string): Promise<any> {
        return axios.get(`/api/article/detail/${id}`)
    }

    /**
     * 删除文章
     * @param id
     */
    static async articleDel(id: string): Promise<any> {
        return axios.delete(`/api/article/del/${id}`)
    }
}
