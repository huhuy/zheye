interface CheckCondition {
    format?: string[];
    size?: number;
}

type ErrorType = 'size' | 'format' | null

export function beforeUploadCheck(file: File, condition: CheckCondition) {
    const {format, size} = condition
    const isValidFormat = format ? format.includes(file.type) : true
    const isValidSize = size ? (file.size / 1024 / 1024 < size) : true
    let error: ErrorType = null
    if (!isValidFormat) {
        error = 'format'
    }
    if (!isValidSize) {
        error = 'size'
    }
    return {
        passed: isValidFormat && isValidSize,
        error
    }
}

/**
 * 字符串是否含有html标签的检测
 * @param htmlStr
 */
export function checkHtml(htmlStr: string) {
    let reg = /<[^>]+>/g;
    return reg.test(htmlStr);
}
