import {createStore} from 'vuex'
import {testData, ColumnProps} from './testData'

export interface UserProps {
    isLogin: boolean;
    nickName?: string;
    id?: string;
    columnId?: string;
    email?: string;
    avatar?: string;
    description?: string;
}

export interface GlobalErrorProps {
    status: boolean;
    message?: string;
}

export interface GlobalDataProps {
    token: string;
    error: GlobalErrorProps;
    loading: boolean;
    columns: ColumnProps[];
    user: UserProps;
}

const store = createStore<GlobalDataProps>({
    state: {
        token: localStorage.getItem('token') || '',
        error: {status: false},
        loading: false,
        columns: testData,
        user: {isLogin: false}
    },
    mutations: {
        setLoading(state, status) {
            state.loading = status
        },
        setError(state, e: GlobalErrorProps) {
            state.error = e
        },
        login(state, user) {
            state.user = {isLogin: true, ...user}
            // localStorage.setItem('user', JSON.stringify(state.user));
        },
        logout(state) {
            state.token = ''
            state.user = {isLogin: false}
            localStorage.clear()
            // delete axios.defaults.headers.common.Authorization
        }
    },
    getters: {
        // currentUser: state => {
        //     let user = localStorage.getItem('user')
        //     if (user) user = JSON.parse(user)
        //     return user || state.user
        // }
    }
})

export default store;
