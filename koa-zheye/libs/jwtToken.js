const jwt = require('jsonwebtoken');
const ApiException = require('./error/apiException')

const SECRET_KEY = 'token'  //密钥，不能丢

const createToken = (payload) => {
    // payload.rtiem = new Date();
    // payload.exp = 60 * 60 * 2 * 1000;
    let token = jwt.sign(payload, SECRET_KEY)
    return token;
}

const tokenExchangeId = (ctx) => {
    if (!ctx.header || !ctx.header.authorization) {
        throw new ApiException(401, '当前用户没登录')
    }
    const parts = ctx.header.authorization.split(' ');
    if (parts.length === 2) {
        const scheme = parts[0];
        const credentials = parts[1];
        if (/^Bearer$/i.test(scheme)) {
            let user = jwt.decode(credentials)
            return user.userId;
        }
    }
    throw new ApiException(401, '登录信息有误')
}

module.exports = {
    createToken, tokenExchangeId
}
