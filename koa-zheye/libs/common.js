const crypto = require('crypto');  // crypto 模块提供了加密功能，实现了包括对 OpenSSL 的哈希、HMAC、加密、解密、签名、以及验证功能的一整套封装。
const fs = require('fs');

const secretKey = 'aes'; //唯一（公共）秘钥
module.exports = {
    // md5 加密方法封装
    md5(buffer) {
        let obj = crypto.createHash('md5'); //创建
        obj.update(buffer);
        return obj.digest('hex'); //16进制
    },
    encrypt(str) { // Base64 编码
        const nameBuffer = Buffer.from(str); // 等同于 Buffer.from(name, "utf-8")
        return nameBuffer.toString("base64");
    },
    decrypt(base64Str) {  // Base64 解码
        const decodeBuffer = Buffer.from(base64Str, "base64"); // 第二个参数就不能省略了
        return decodeBuffer.toString("utf-8");
    },
    pageResult(list, total = 0, page = 1, size = 10) {
        const pages = Math.ceil(total / size);
        return {list, total, pages, currentPage: Number(page), size: Number(size)}
    },
    unlink(path) {
        return new Promise((resolve, reject) => {
            fs.unlink(path, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve();
                }
            })
        })
    },
    // 接口返回数据方法封装
    handleResult(code, data, msg, page, token) {
        if (page) {
            return {code, data, msg, page}
        }
        if (token) {
            return {code, data, msg, token}
        }
        return {code, data, msg}
    }
}
