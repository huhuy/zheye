class ApiException extends Error {
  constructor(code = 500, msg = '处理失败',) {
    super()
    this.code = code
    this.msg = msg
  }
}

module.exports = ApiException;