const statusCode = {
  SUCCESS: (msg, data) => ({
    code: 200,
    msg,
    data,
  }),
  ERROR_SQL: (msg) => ({ // 访问数据库异常
    code: 201,
    msg,
  }),
  ERROR_EXISTED: (msg) => ({ // 用户已经存在
    code: 204,
    msg,
  }),
  ERROR_LOGIN: (msg) => ({ // 用户不存在
    code: 205,
    msg,
  }),
  ERROR_AUTH: (msg, err) => ({ // 系统权限错误
    code: 401,
    msg,
    err
  }),
  ERROR_LOSER: (msg, err) => ({ // 处理失败
    code: 500,
    msg,
    err
  }),
  ERROR_ARG: (msg) => ({ // 传参错误
    code: -1,
    msg,
  }),
};
module.exports = statusCode


// 200 OK - [GET]：服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）。
// 400 INVALID REQUEST - [POST/PUT/PATCH]：用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的。
// 401 Unauthorized - [*]：表示用户没有权限（令牌、用户名、密码错误）。
// 403 Forbidden - [*] 表示用户得到授权（与401错误相对），但是访问是被禁止的。
// 404 NOT FOUND - [*]：用户发出的请求针对的是不存在的记录，服务器没有进行操作，该操作是幂等的。
// 406 Not Acceptable - [GET]：用户请求的格式不可得（比如用户请求JSON格式，但是只有XML格式）。
// 422 Unprocesable entity - [POST/PUT/PATCH] 当创建一个对象时，发生一个验证错误。
// 500 INTERNAL SERVER ERROR - [*]：服务器发生错误，用户将无法判断发出的请求是否成功。
