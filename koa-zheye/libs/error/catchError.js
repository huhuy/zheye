const ApiException = require('./apiException')
const statusCode = require("./statusCode");

//异常中间件
const catchError = async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Headers', 'Content-Type,Access-Control-Allow-Headers,Authorization,X-Requested-With')
  ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  try {
    if (ctx.method === 'OPTIONS') {
      ctx.body = 200;
    } else {
      await next();
    }
    // await next();
  } catch (err) {
    console.log('全局异常err------->  ', err)
    if (err instanceof ApiException) {
      return ctx.body = err
    } else if (err.status === 401) {
      return ctx.body = statusCode.ERROR_AUTH("请登录", err.message);
    } else {
      return ctx.body = statusCode.ERROR_LOSER("处理失败", err.message);
    }
  }
}

module.exports = catchError
