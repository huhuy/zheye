const router = require('koa-router')()
const uploadFile = require("../api/uploadTencent")
// 路由入口
// 登录注册接口
router.use('/api/user', require('../api/user'));
// 专栏信息
router.use('/api/category', require('../api/category'));
// 文章信息
router.use('/api/article', require('../api/article'));

// 上传图片
router.post('/api/tencent/upload', uploadFile.uploadImgTen)


router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

module.exports = router
