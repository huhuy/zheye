const config = require('./config');
const mysql = require('mysql-pro');

// 连接数据库文件
//使用new来初始化mysql
const db = new mysql({
    mysql: {
        host: config.dbHost,
        port: config.dbPort,
        user: config.dbUser,
        password: config.dbPass,
        database: config.dbName,
        timezone: config.timezone
    }
});
module.exports = db

