const Router = require('koa-router');
const common = require('../libs/common');
const statusCode = require("../libs/error/statusCode");
const uuid = require("node-uuid");
let router = new Router();
// 添加token 用的
const {createToken, tokenExchangeId} = require('../libs/jwtToken');

// 注册接口
router.post('/register', async ctx => {
    const {username, password, email} = ctx.request.body;
    // 密码加密
    const password_md = common.md5(ctx.config.ADMIN_PREFIX + password);
    let data = await ctx.db.query(`SELECT * FROM admin`);
    let isTrue = data.findIndex(item => item.email === email)
    if (isTrue !== -1) return ctx.body = statusCode.ERROR_SQL('此邮箱已被注册')
    // 开启事务
    await ctx.db.startTransaction();
    // 新增用户
    const userId = uuid.v1().replace(/-/g, "");
    await ctx.db.executeTransaction(`INSERT INTO admin (id, nickname, email, password) VALUES("${userId}","${username}","${email}","${password_md}")`)
    // 创建一个专栏
    const columnId = uuid.v1().replace(/-/g, "");
    const desc = `这里是${username}的专栏，有一段非常有意思的简介，可以更新一下欧`
    await ctx.db.executeTransaction(`INSERT INTO category (id, author, title, description) VALUES("${columnId}","${userId}","${username}的专栏","${desc}")`)
    // 关闭事务
    await ctx.db.stopTransaction();
    ctx.body = statusCode.SUCCESS('注册成功')
})

// 登录接口
router.post('/login', async ctx => {
    let {email, password} = ctx.request.body;
    const data = await ctx.db.query(`SELECT * FROM admin WHERE email = '${email}'`);
    const result = data[0]
    if (result && result instanceof Object) {
        let pass = common.md5(ctx.config.ADMIN_PREFIX + password);
        if (pass !== result.password) return ctx.body = statusCode.ERROR_LOSER('密码不正确')
        // 生成Token
        let token = createToken({email: email, username: result.nickname, userId: result.id})
        ctx.body = statusCode.SUCCESS('登录成功', {token})
    } else {
        ctx.body = statusCode.ERROR_LOGIN('该用户不存在')
    }
})

// 获取当前用户登录信息
router.get('/current', async ctx => {
    const id = tokenExchangeId(ctx);
    const sql = `SELECT a.id,c.id AS columnId,a.avatar,a.nickname,a.email,a.description,a.created_at FROM admin a LEFT JOIN category c ON a.id=c.author WHERE a.id = '${id}'`
    const data = await ctx.db.query(sql);
    const result = data[0];

    if (result && result instanceof Object) {
        const {email, nickname, description, avatar, columnId, createdAt} = result
        ctx.body = statusCode.SUCCESS('请求成功', {id, email, nickname, description, avatar, columnId, createdAt})
    } else {
        ctx.body = statusCode.ERROR_LOGIN('用户未登录')
    }
})

//更新单个用户信息
router.patch(`/update/:id`, async ctx => {
    const id = ctx.params.id
    const {nickname, description, avatar} = ctx.request.body;
    const sql = `update admin set nickname= '${nickname}', description= '${description}',avatar= '${avatar}' where id = "${id}" `
    const result = await ctx.db.query(sql);
    if (result && result instanceof Object) {
        ctx.body = statusCode.SUCCESS('更新成功', {id, nickname, description, avatar})
    }
})

// 测试接口
router.get('/test', async ctx => {
    let data = '测试的啊'
    ctx.body = statusCode.SUCCESS('请求成功', data)
})

module.exports = router.routes()
