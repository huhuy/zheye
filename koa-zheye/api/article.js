const router = require('koa-router')();
const statusCode = require("../libs/error/statusCode");
const uuid = require("node-uuid");
const common = require("../libs/common");
const md = require('markdown-it')();

// 文章列表
router.get('/:id/list', async ctx => {
    const {page, size} = ctx.query // 获取 url 里传过来的参数
    const columnId = ctx.params.id
    const data = await ctx.db.query(`SELECT * FROM article where column_id = '${columnId}' ORDER BY created_at DESC LIMIT ${(page -1) *size},${size}`);
    const result = data.map(obj => {
        obj.excerpt = common.decrypt(obj.excerpt)
        return obj
    })
    const total = await ctx.db.query(`SELECT COUNT(*)  FROM article where column_id = '${columnId}'`)
    ctx.body = statusCode.SUCCESS('请求成功', common.pageResult(result, total[0]['COUNT(*)'], page, size))
});

// 添加文章
router.post('/add', async ctx => {
    const id = uuid.v1().replace(/-/g, "");
    const {title, content, image, author, columnId} = ctx.request.body;
    let excerpt = md.render(content).replace( /(<([^>]+)>)/ig, "");
    excerpt = excerpt.length > 110 ? excerpt.substr(0, 110) + '...' : excerpt;
    const sql = `INSERT INTO article(id, title, excerpt, content, image, author, column_id)
                             VALUES ("${id}","${title}","${common.encrypt(excerpt)}","${common.encrypt(content)}","${image}","${author}","${columnId}")`
    let data = await ctx.db.query(sql);
    let result = await ctx.db.query(`SELECT * FROM article WHERE id = '${id}'`)
    ctx.body = statusCode.SUCCESS('添加成功', result[1])
});

// 更新文章
router.put('/update/:id', async ctx => {
    const  id= ctx.params.id
    const {title, content, image} = ctx.request.body;
    let excerpt = md.render(content).replace( /(<([^>]+)>)/ig, "");
    excerpt = excerpt.length > 110 ? excerpt.substr(0, 110) + '...' : excerpt;
    console.log('id', id)
    const sql = `update article set title= '${title}', excerpt= '${common.encrypt(excerpt)}',content= '${common.encrypt(content)}',image= '${image}' where id = "${id}" `
    let data = await ctx.db.query(sql)
    ctx.body = statusCode.SUCCESS('更新成功', {id})
})

// 获取文章详情
router.get('/detail/:id', async ctx => {
    const id = ctx.params.id
    const sql = `SELECT c.id, c.title, c.excerpt, c.content, c.image, c.column_id, c.created_at, a.id AS author, a.nickname, a.avatar, a.description 
                 FROM article c LEFT JOIN admin a ON c.author = a.id  WHERE c.id='${id}'`
    let data = await ctx.db.query(sql)
    let result = data[0]
    result.excerpt = common.decrypt(result.excerpt)
    result.content = common.decrypt(result.content)
    ctx.body = statusCode.SUCCESS('请求成功', result)
});

// 删除文章
router.delete('/del/:id', async ctx => {
    const id = ctx.params.id
    const column = await ctx.db.query(`SELECT column_id FROM article where id = '${id}' `)
    const data = await ctx.db.query(`DELETE FROM article WHERE id='${id}'`)
    ctx.body = statusCode.SUCCESS('删除成功', {column: column[0].column_id})
});

module.exports = router.routes()
