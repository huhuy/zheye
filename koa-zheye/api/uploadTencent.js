/*
 * 上传图片到腾讯云
 */
const fs = require('fs')
const COS = require('cos-nodejs-sdk-v5');
const statusCode = require("../libs/error/statusCode");
const TENCENT = require('../config/tencent')

const cos = new COS({
    SecretId: TENCENT.secretId,
    SecretKey: TENCENT.secretKey
});

const saveFile = (fileName, path) => {
    return new Promise((resolve, reject) => {
        let reader = fs.createReadStream(path); // 创建可读流
        let params = {
            Bucket: TENCENT.Bucket,
            Region: TENCENT.Region,
            Key: fileName,
            Body: reader
            // ContentLength: fs.statSync(path).size
        }
        cos.putObject(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data)
            }
        });
    })
}
const uploadImgTen = async (ctx) => {
    // 前端必须以formData格式进行文件的传递
    let file = ctx.request.files.file; // 获取上传文件
    if (file) {
        let ext = file.name.split('.').pop() // 获取文件后缀
        let path = file.path  // 获取文件地址
        let fileName = `image-${Date.now()}.${ext}`; // 保存在cos的文件名
        let result = await saveFile(fileName, path)
        console.log('result', result)
        if (result) {
            ctx.body = statusCode.SUCCESS("上传成功！", {url: 'https://' + result.Location});
        } else {
            ctx.body = statusCode.ERROR_LOSER("上传失败！");
        }
    } else {
        ctx.body = statusCode.ERROR_ARG("没有选择图片");
    }
}

module.exports = {
    uploadImgTen
}
