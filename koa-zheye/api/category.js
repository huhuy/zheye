const router = require('koa-router')();
const statusCode = require("../libs/error/statusCode");
const common = require("../libs/common");

// 专栏列表
router.get('/list', async ctx => {
    const {page, size} = ctx.query // 获取 url 里传过来的参数
    console.log(ctx.query)
    const sql = `SELECT c.id,c.author,c.title,c.description,c.image, c.created_at, a.id AS userId
                 FROM category c LEFT JOIN admin a ON c.author = a.id 
                 GROUP BY c.id ORDER BY created_at ASC LIMIT ${(page -1) *size},${size};`
    const data = await ctx.db.query(sql)
    const total = await ctx.db.query(`SELECT COUNT(*)  FROM category`)
    ctx.body = statusCode.SUCCESS('请求成功', common.pageResult(data, total[0]['COUNT(*)'], page, size))
})

// 获得一个专栏详情
router.get(`/detail/:id`, async ctx => {
    const {id} = ctx.params
    let data = await ctx.db.query(`SELECT id, title, description,image,author,created_at FROM category WHERE id='${id}'`)
    ctx.body = statusCode.SUCCESS('请求成功', data[0])
})

// 更新专栏
router.patch(`/update/:id`, async ctx => {
    const id = ctx.params.id
    const {title, description, image} = ctx.request.body;
    const sql = `update category set title= '${title}', description= '${description}',image= '${image}' where id = "${id}" `
    const result = await ctx.db.query(sql);
    if (result && result instanceof Object) {
        ctx.body = statusCode.SUCCESS('更新成功', {id, title, description, image})
    }
})
module.exports = router.routes()
