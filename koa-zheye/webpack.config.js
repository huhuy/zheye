const webpack = require('webpack');
const path = require('path');
const babelpolyfill = require("babel-polyfill");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const MinifyPlugin = require('babel-minify-webpack-plugin');

// https://github.com/193Eric/koa2-blog
module.exports = {
  entry: path.resolve(__dirname, 'server.js'),
  output: {
    path:path.resolve(__dirname,'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['es2015'] //兼容es6，并添加.babelrc
        }
      }]

    }]
  },
  context: __dirname,
  node: {
    console: true,
    global: true,
    process: true,
    Buffer: true,
    __filename: true,
    __dirname: true,
    setImmediate: true,
    path: true
  },
  target: 'node', // 服务端打包
  externals: [nodeExternals()],
  resolve: {
    modules: [
      'node_modules'
    ],
    alias: {
      model: require('path').resolve(__dirname, 'models')
    }
  },
  plugins: [
    // 环境区分
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }

      // DEV: JSON.stringify('development'), // production
      // flag: 'true',
      // calc: '1 + 1'
    }),
    new CleanWebpackPlugin(),
    new MinifyPlugin() //压缩js
  ]
};