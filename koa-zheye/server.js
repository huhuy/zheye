const Koa = require('koa');
const app = new Koa();
const koaBody = require("koa-body");
const config = require('./config/config');
const db = require('./config/db');
const json = require('koa-json');
const koaStatic = require('koa-static');
const bodyparser = require('koa-bodyparser');
const logger = require('koa-logger');
const onerror = require('koa-onerror');
// const cors = require('koa2-cors');
const path = require("path");

const catchError = require('./libs/error/catchError')
app.use(catchError) //一定要放在第一个中间件

/* 数据库连接并且挂载到context 对象上  */
app.context.db = db;
app.context.config = config;

// error handler
onerror(app)

//全局变量
global.SECRET_KEY = 'token'//定一个秘钥 jwt

//全局校验token
// app.use(koaJwt({
//     secret: global.SECRET_KEY
// }).unless({
//     // 登录，注册接口不需要验证
//     path: [/^\/login/, /^\/register/]
// }));

// 配置上传文件的信息
app.use(
    koaBody({
        multipart: true,
        formidable: {
            maxFileSize: 2000 * 1024 * 1024 // 设置上传文件大小最大限制，默认2M
        }
    })
);

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}));
app.use(logger());
app.use(json());

// logger
app.use(async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    console.log(`logger----> ${ctx.method} ${ctx.url} - ${ms}ms`)
});

// 静态文件 koaStatic 在 koa-router 的其他规则之上
app.use(koaStatic(path.resolve('dist'))); // 将 webpack 打包好的项目目录作为 Koa 静态文件服务的目录

/**
 * koa2-cors解决跨域
 * 相关博客地址
 * https://blog.csdn.net/shi851051279/article/details/99617908
 *  */
// app.use(cors({
//     origin: function (ctx) {
//         return '*'
//     },
//     exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
//     maxAge: 5,
//     credentials: true,
//     allowMethods: ['GET', 'POST', 'DELETE', 'OPTIONS', 'PUT'],
//     allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
// }))

const router = require ('./routes/index');
// 路由
app.use(router.routes()); // 将路由规则挂载到Koa上。

app.listen(3300, () => {
    console.log('Koa is listening on port 3300');
});
